## Smart types

---slide---

## Taking a look back

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">/**
* @throws: ArithmeticException when b is 0.0
*/
fun divide(a : Double, b : Double) : Double

fun divide(a : Double, b : Double) : Double?

fun divide(a : Double, b : Double) : Either&lt;String, Double>

</code>
</pre>

---slide---

## Programming by contract

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">/**
* Pre: b must be positive
*/
fun divide(a : Double, b : Double) : Double



fun foo(b : Double){
  if(b > 0){
    val result = divide(10, b)
  }
}

</code>
</pre>

---slide---

## Using the type system

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">data class Positive(val value : Double) {
  init {
    require(value > 0)
  }
}


fun divide(a : Double, b : Positive) : Double {
  return a / b.value
}

</code>
</pre>

---slide---

## The problem

* We are back to a runtime exception

---slide---

## Removing runtime exceptions

* Add the Arrow Analysis (gradle) plugin

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">fun main() {
  println(Positive(-1))
}

</code>
</pre>

* Press compile

```
pre-condition `value > 0` is not satisfied in `Positive(-1)`
  -> unsatisfiable constraint: `-1 > 0`
  -> `-1` bound to param `value` in `Positive.&lt;init>` at Main.kt
  -> in branch: 1 != null
```

---slide---

## Arrow Analysis plugin

* Pre/Post conditions
	* Numbers
	* Strings
* "Nullability like" compile time check	
* Laws
	* Collections
* Currently only supports gradle

---slide---

## Pre/Post conditions

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">fun foo(x: Int): Int {
  require(x > 0) { "value > 0" } //Or pre
  return (x + 1).post({ it > 1 }) { "result > 1" }
}

</code>
</pre>

---slide---

## Pre/Post conditions

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">fun foo(x: Int): Int {
  require(x > 0) { "value > 0" } //Or pre
  return (x + 1).post({ it > 1 }) { "result > 1" }
}

fun bar(value : Int){
  foo(0) //Does not compile
  foo(value) //Does not compile
	
  if(value > 0){
    val myPositiveInt = foo(value) //Compiles
	foo(myPositiveInt) //Compiles
  }
}

</code>
</pre>

---slide---

## Pre/Post conditions as a class

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">@JvmInline
value class Positive(val value: Double) {
  init { require(value > 0) }
}

fun foo(x: Positive): Positive {
  return Positive(x.value + 1)
}

</code>
</pre>

---slide---

## Checking strings

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">@JvmInline
value class String10(val value: String) {
  init {
    require(value.length <= 10) { "Maximum 10 characters" }
  }
}

fun bar(value : String){
  String10("123456789011") //Does not compile
  String10(value) //Does not compile

  if(value.length <= 10){
    String10(value) //Compiles
  }
}

</code>
</pre>

---slide---

## Checking collections

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">fun foo(myList : List&lt;String>){
  println(myList.first()) //Does not compile

  //Compiles
  if(myList.isNotEmpty()){
    println(myList.first())
  }
}

</code>
</pre>

* But why does this work?

---slide---

## Laws

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">@Laws
object ListLaws {

  @Law
  inline fun &lt;E> List&lt;E>.firstLaw(): E {
    pre(size >= 1) { "not empty" }
    return first()
  }
  
}

</code>
</pre>

---slide---

## The good parts

* Add constrains to your domain model
	* Database field length
	* External services restrictions
	* Make illegal states non-representable

---slide---

## The problems

* Still somewhat limited (Int, String, List)
* It is VERY strict (but you can opt out)
* Does not work with deserialization/reflection &#8594; domain only
	* Jackson
	* Hibernate
* Does not support streaming api well

---slide---

## Transforming lists

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:150%">//Does not work
listOf(1)
  .filter { it > 0 } //information is not saved
  .map { Positive(it) }

//Works
listOf(1)
  .mapNotNull { if (it > 0) Positive(it) else null }
  
</code>
</pre>

---slide---


## What did we learn

<img style="height:500px;float:right;" src="slides/images/cherry.jpg">

* Move runtime errors to compile time
* Make illegal states non-representable
* Cherry on top