## Concluding

* We can have both context and an error type
* We can have both compiler support and readability
* We can have both immutability and ease of change
* We can move runtime exceptions to compile time 

---slide---

## Conclusion

<img style="height:500px" src="slides/images/cakes.jpg">

---slide---

## Keep in touch

<img style="float: right;" src="slides/images/photo.jpg">

* ties_ven @Twitter
* Ties van de Ven @LinkedIn
* www.tiesvandeven.nl
* Or talk to me in real life
* Sheets: https://tiesvandeven.gitlab.io/fpinarrow/