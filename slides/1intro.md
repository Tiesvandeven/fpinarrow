## whoami
<img style="float: right;" src="slides/images/photo.jpg">

* Ties van de Ven
* Software Engineer

---slide---

## The monolith

<img style="height:500px" src="slides/images/13273990_m.jpg">

---slide---

## Discovering FP

<img style="height:500px" src="slides/images/18737649_m.jpg">


---slide---

## Agenda

JIT FP theory = *__TODO()__*

Monads (Either)= *__TODO()__*

Real world monads = *__TODO()__*

Optics = *__TODO()__*

Smart types = *__TODO()__*
